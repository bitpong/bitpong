var OnlineUsers = new Meteor.Collection('onlineUsers');
var Games = new Meteor.Collection('games');
var gameRunned = false;

function isReady(game) {
  return game && game.master && game.slave;
}

function cleanGame() {
  gameRunned = false;
  $('canvas').remove();
}

function resetGame(game) {
  cleanGame();
  Meteor.call('joinGame');
}

function isMaster(gameInfo) {
  return Meteor.userId() === gameInfo.master;
}

Deps.autorun(function () {
  Meteor.subscribe('onlineUsers');
  Meteor.subscribe('games');
});

Meteor.users.find().observe({
  added: function (user) {
    // The user is now logged. Join a game.
    Meteor.call('joinGame');
  },
  removed: function (user) {
    // Logout. Clean the game.
    cleanGame();
  }
});

Games.find().observe({
  added: function (game) {
    if (isReady(game)) {
      runGame(game);
    }
  },
  changed: function (game) {
    if (isReady(game)) {
      runGame(game);
    }
  },
  removed: function (game) {
    Meteor.call('resetGame', game, function () {
      resetGame();
    });
  }
});

Meteor.setInterval(function () {
  Meteor.call('keepAlive', Meteor.userId(), function () {});
}, 1000);

Template.onlineNavbarItem.onlinePlayers = function () {
  return OnlineUsers.find().count();
};

Template.body.currentUser = function () {
  return Meteor.userId();
};

Template.body.currentGame = function () {
  return Games.findOne();
};

Template.score.currentGame = function () {
  return Games.findOne();
};

Template.score.state = function () {
  var game = Games.findOne();
  if (!game) { return; }

  if (game.score.master >=3) {
    cleanGame();
    return isMaster(game) ? 'You win!' : 'You lost :-(';
  } else if (game.score.slave >=3) {
    cleanGame();
    return isMaster(game) ? 'You lost :-(' : 'You win!';
  }
};

function runGame (gameInfo) {
  if (gameRunned) { return; }
  gameRunned = true;

  var computerBetSpeed = 190;
  var ballSpeed = 400;
  var ballReleased = false;
  var playerBet = null;

  var game = new Phaser.Game(480, 640, Phaser.AUTO, 'phaser-element', {
    preload: preload,
    create: create,
    update: update
  });

  function checkGoal() {
    if (ball.y < 15) {
      setBall();
      Meteor.call('goal', {
        who: 'master',
        game: gameInfo
      });
    } else if (ball.y > 625) {
      setBall();
      Meteor.call('goal', {
        who: 'slave',
        game: gameInfo
      });
    }
  }

  function setBall() {
    if (ballReleased) {
      ball.x = game.world.centerX;
      ball.y = game.world.centerY;
      ball.body.velocity.x = 0;
      ball.body.velocity.y = 0;
      ballReleased = false;
    }
  }

  function ballHitsBet (_ball, _bet) {
    var diff = 0;

    if (_ball.x < _bet.x) {
      //If ball is in the left hand side on the racket
      diff = _bet.x - _ball.x;
      _ball.body.velocity.x = (-10 * diff);
    } else if (_ball.x > _bet.x) {
      //If ball is in the right hand side on the racket
      diff = _ball.x -_bet.x;
      _ball.body.velocity.x = (10 * diff);
    } else {
      //The ball hit the center of the racket, let's add a little bit of a tragic accident(random) of his movement
      _ball.body.velocity.x = 2 + Math.random() * 8;
    }
  }

  function releaseBall() {
    if (!ballReleased) {
      ball.body.velocity.x = ballSpeed;
      ball.body.velocity.y = -ballSpeed;
      ballReleased = true;
    }
  }

  function createBet(x, y) {
    var bet = game.add.sprite(x, y, 'bet');
    bet.anchor.setTo(0.5, 0.5);
    bet.body.collideWorldBounds = true;
    bet.body.bounce.setTo(1, 1);
    bet.body.immovable = true;

    return bet;
  }

  function preload() {
    game.load.image('bet', '/img/bet.png');
    game.load.image('ball', '/img/ball.png');
    game.load.image('background', '/img/background.png');
  }

  function create() {
    game.add.tileSprite(0, 0, 480, 640, 'background');

    playerBet = createBet(game.world.centerX, 600);
    computerBet = createBet(game.world.centerX, 20);


    ball = game.add.sprite(game.world.centerX, game.world.centerY, 'ball');
    ball.anchor.setTo(0.5, 0.5);
    ball.body.collideWorldBounds = true;
    ball.body.bounce.setTo(1, 1);

    if (Meteor.userId() === gameInfo.master) {
      game.input.onDown.add(releaseBall, this);
    }
  }

  Meteor.setInterval(function () {
    if (!Meteor.userId() || !gameRunned || !playerBet || !ball) { return; }

    Meteor.call('updatePosition', {
      game: gameInfo,
      position: {
        x: playerBet.position.x,
        y: playerBet.position.y
      },
      isMaster: isMaster(gameInfo)
    }, function () {});

    if (isMaster(gameInfo)) {
      Meteor.call('updateBallPosition', {
        game: gameInfo,
        position: {
          x: ball.position.x,
          y: ball.position.y
        }
      }, function () {});
    } else {
      Meteor.call('getBallPosition', {
        game: gameInfo
      }, function (err, position) {
        if (position) {
          ball.x = position.x;
          ball.y = -position.y + game.world.height;
        }
      });
    }
  }, 30);

  function update() {
    playerBet.x = game.input.x;

    Meteor.call('getEnemyPosition', {
      game: gameInfo,
      isMaster: isMaster(gameInfo)
    }, function (err, enemyPosition) {
      if (enemyPosition) {
        computerBet.x = enemyPosition.x;
      }
    });

    var playerBetHalfWidth = playerBet.width / 2;

    if (playerBet.x < playerBetHalfWidth) {
      playerBet.x = playerBetHalfWidth;
    } else if (playerBet.x > game.width - playerBetHalfWidth) {
      playerBet.x = game.width - playerBetHalfWidth;
    }

    //Check and process the collision ball and racket
    if (isMaster(gameInfo)) {
      game.physics.collide(ball, playerBet, ballHitsBet, null, this);
      game.physics.collide(ball, computerBet, ballHitsBet, null, this);
    }

    if (isMaster(gameInfo)) {
      checkGoal();
    }
  }
}

var Games = new Meteor.Collection('games');
var OnlineUsers = new Meteor.Collection('onlineUsers');

var positions = {};

Games.find().forEach(function (game) {
  positions[game._id] = {
    master: null,
    slave: null,
    mouse: null
  };
});

// Cleans the onlineUsers collection every minutes.
Meteor.setInterval(function () {
  var query = {
    lastSeen: { $lt: new Date(new Date() - 5000) }
  };

  var outdated = OnlineUsers.find(query).fetch();
  if (outdated.length) {
    OnlineUsers.remove(query);
  }

  outdated.forEach(function (onlineUser) {
    Games.remove({ $or: [
      { master: onlineUser.userId },
      { slave: onlineUser.userId }
    ]});
  });

}, 1000);

Meteor.publish('onlineUsers', function () {
  return OnlineUsers.find();
});

Meteor.publish('users', function () {
  return Meteor.users.find({ _id: this.userId });
});

Meteor.publish('games', function () {
  if (!this.userId) { return; }

  return Games.find({ $or: [
    { master: this.userId },
    { slave: this.userId }
  ]});
});

Meteor.methods({
  keepAlive: function (userId) {
    if (!userId) { return; }

    OnlineUsers.upsert({ userId: userId }, {
      userId: userId,
      lastSeen: new Date()
    });
  },
  joinGame: function () {
    var userId = Meteor.userId();

    if (userId) {
      var game = Games.findOne({
        master: { $ne: null },
        slave: null
      });

      if (game) {
        if (game.master !== userId) {
          Games.update({ _id: game._id }, { $set:{ slave: userId }});
          game = Games.findOne({ _id: game._id });
        }
      } else {
        var _id = Games.insert({
          master: userId,
          slave: null,
          score: {
            master: 0,
            slave: 0
          }
        });

        game = Games.findOne({ _id: _id });
      }

      if (!positions[game._id]) {
        positions[game._id] = {
          master: null,
          slave: null,
          mouse: null
        };
      }

      return game;
    }

    return null;
  },
  updatePosition: function (data) {
    if (data.isMaster) {
      positions[data.game._id].master = data.position;
    } else {
      positions[data.game._id].slave = data.position;
    }
  },
  updateBallPosition: function (data) {
    positions[data.game._id].mouse = data.position;
  },
  getBallPosition: function (data) {
    return positions[data.game._id].mouse;
  },
  getEnemyPosition: function (data) {
    return data.isMaster ? positions[data.game._id].slave :
      positions[data.game._id].master;
  },
  goal: function (data) {
    var key = 'score.' + data.who;
    var update = {};
    update[key] = 1;

    Games.update({
      master: data.game.master,
      slave: data.game.slave
    }, {
      $inc: update
    });
  },
  resetGame: function (game) {
    positions[game._id] = {
      master: null,
      slave: null,
      mouse: null
    };
  }
});
